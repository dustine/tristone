# [Tristone](https://dustine.gitlab.io/tristone)

## Project setup

```console
npm install
```

### Compiles and hot-reloads for development

```console
npm run serve
```

### Compiles and minifies for production

```console
npm run build
```

### Run your unit tests

```console
npm run test:unit
```

### Lints and fixes files

```console
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
