/**
 *  Length of 1 Cardinal Day
 *    1229413 Hours == 21621600 Instances?
 *
 *  The Cardinal Day at Midnight 1st of First 1228/Midnight 1st of January 2013:
 *    657,031,560.51976(...) with 365
 *    6574 8158 2.13655(...) with 324.(6), supposedly
 *  Age of Main Universe:
 *    92210404328 (La'riena?) years
 *
 * Strategy:
 * - ...
 * - to 1/1/1228
 * - ...
 */
