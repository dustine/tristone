const modulo = function(m: number, d: number): number {
  const rem = (m % d) * Math.sign(d);
  return Math.abs(rem < +0 ? (Math.abs(d) + rem) % d : rem) * Math.sign(d);
};

export default modulo;
