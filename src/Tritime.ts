import modulo from "./modulo";

enum TriWeekday {
  "Adraki",
  "Tyriel",
  "Arystag",
  "Kismeran",
  "Ursval",
  "Ryrsgar",
  "Karien"
}

enum TriMonth {
  "Ghorza" = 1,
  "Ihgid",
  "Braugh",
  "Rhegrel",
  "Tazvar",
  "Kezri",
  "Oriarg",
  "Shayagor",
  "Azgral"
}

let secondsInMoment = 90;
let momentsInHour = 40;
let hoursInDay = 27;
let daysInWeek = 7;
let daysInYear = 324;
let leapDays = 8;
let yearsInCycle = 12;

let daysByMonth = [12, 41, 40, 40, 41, 40, 41, 41, 28];
// int. 0,1,2,3,4,5,6,7,8
// reg. 2,3,4,5,6,7,8,1,2
// leap 9,3,4,5,6,7,8,1,2
let daysByMonthOnLeap = daysByMonth
  .slice()
  .map((val, i) => (i == 0 ? val + leapDays : val));

let secondsInHour = momentsInHour * secondsInMoment;
let secondsInDay = hoursInDay * secondsInHour;
let monthsInYear = daysByMonth.length - 1;

let daysInCycle = yearsInCycle * daysInYear + leapDays;
let secondsInCycle = daysInCycle * secondsInDay;

class Tridate {
  // Epoch:  2013-03-19 15:00:00 <-> 1229-9-01 00:00:00
  // so that 2013-01-01 00:00:00 <-> 1228-1-01 00:00:00
  public static readonly epoch = new Date(2013, 2, 19, 15, 0, 0);

  protected delta: number = 0;
  protected _second: number = 0;
  protected _moment: number = 0;
  protected _hour: number = 0;
  protected _day: number = 0;
  protected _month: number = 0;
  protected _year: number = 0;

  public get second(): number {
    return this._second;
  }
  public get moment(): number {
    return this._moment;
  }
  public get hour(): number {
    return this._hour;
  }
  public get date(): number {
    if (this._month == 0 && !this.isLeapYear())
      return this._day + daysByMonth[monthsInYear] + 1;
    return this._day + 1;
  }
  public get day(): TriWeekday {
    return Math.floor(modulo(this.delta / secondsInDay, daysInWeek));
  }
  // public get week(): number {
  // TODO
  // }
  public get month(): number {
    if (this._month == 0) {
      if (this.isLeapYear()) return monthsInYear + 1;
      else return 2;
    }
    return ((this._month + 1) % monthsInYear) + 1;
  }
  public get year(): number {
    return this._year + 1229;
  }

  public isLeapYear(): boolean {
    return this._year % 12 == 0;
  }

  private getDaysByMonth() {
    return this.isLeapYear() ? daysByMonthOnLeap : daysByMonth;
  }

  public maxDate(): number {
    return this.getDaysByMonth()[this._month];
  }

  public maxMonth(): number {
    return monthsInYear + (this.isLeapYear() ? 1 : 0);
  }

  constructor(
    year?: number,
    month?: TriMonth,
    days = 1,
    hours = 0,
    moments = 0,
    seconds = 0
  ) {
    if (year === undefined) return;
    if (month === undefined) {
      this.offset(year);
    } else {
      const cycles = Math.floor((year - this.year) / yearsInCycle);
      const forwardYears = modulo(year - this.year, yearsInCycle);

      let dateDays = 0;
      // month processing
      let rawMonth = [undefined, 7, undefined, 1, 2, 3, 4, 5, 6, 0][month];

      if (!(rawMonth == 0 && forwardYears == 0)) {
        // non-leap year after the leap week
        dateDays += leapDays;
      }
      if (rawMonth === undefined) {
        if (days <= daysByMonth[8]) {
          // end of year
          rawMonth = 8;
        } else {
          // start of year on non-leap
          rawMonth = 0;
          dateDays -= daysByMonth[8];
        }
      }
      dateDays += daysByMonth
        .slice(0, rawMonth)
        .reduce((cum, val) => cum + val, 0);
      // year processing

      dateDays += forwardYears * daysInYear;

      this.offset(seconds, moments, hours, days - 1 + dateDays, cycles);
    }
  }

  static fromTerran(date: Date, offset: number = 0) {
    return new Tridate(
      (date.getTime() - Tridate.epoch.getTime()) / 1000 + offset
    );
  }

  public getTerran() {
    return new Date(Tridate.epoch.getTime() + this.delta * 1000);
  }

  public getTime() {
    return this.delta;
  }

  protected offset(
    seconds: number,
    moments = 0,
    hours = 0,
    days = 0,
    cycles = 0
  ) {
    let offset =
      Math.trunc(seconds) +
      days * secondsInDay +
      moments * secondsInMoment +
      hours * secondsInHour +
      cycles * secondsInCycle;

    if (Math.abs(offset) < 1) return;

    this.delta += offset;

    let rewind = offset < 0;
    if (rewind) {
      offset = -offset;
    }

    if (!rewind) {
      while (offset >= secondsInCycle) {
        this._year += yearsInCycle;
        offset -= secondsInCycle;
      }
    } else {
      while (offset >= secondsInCycle) {
        this._year -= yearsInCycle;
        offset -= secondsInCycle;
      }
    }

    // TODO condense this, as we could add, like... years at once.
    while (offset >= secondsInDay) {
      this.addDay(rewind);
      offset -= secondsInDay;
    }

    while (offset >= secondsInHour) {
      this.addHour(rewind);
      offset -= secondsInHour;
    }

    while (offset >= secondsInMoment) {
      this.addMoment(rewind);
      offset -= secondsInMoment;
    }

    while (--offset >= 0) {
      this.addSecond(rewind);
    }
  }

  protected addDay(negative: boolean = false) {
    const increment = negative ? -1 : 1;
    this._day += increment;

    const daysByMonth = this.getDaysByMonth();
    if (this._day >= daysByMonth[this._month]) {
      // new month
      this._day = 0;
      if (++this._month >= daysByMonth.length) {
        // new year
        this._month = 0;
        ++this._year;
      }
    } else if (this._day < 0) {
      // new month
      if (--this._month < 0) {
        // new year
        --this._year;
        this._month = daysByMonth.length - 1;
      }
      this._day = daysByMonth[this._month] - 1;
    }
  }

  protected addHour(negative: boolean = false) {
    const increment = negative ? -1 : 1;
    this._hour += increment;

    if (this._hour >= hoursInDay) {
      this._hour = 0;
      this.addDay(negative);
    } else if (this._hour < 0) {
      this._hour = hoursInDay - 1;
      this.addDay(negative);
    }
  }

  protected addMoment(negative: boolean = false) {
    const increment = negative ? -1 : 1;
    this._moment += increment;

    if (this._moment >= momentsInHour) {
      this._moment = 0;
      this.addHour(negative);
    } else if (this._moment < 0) {
      this._moment = momentsInHour - 1;
      this.addHour(negative);
    }
  }

  protected addSecond(negative: boolean = false) {
    const increment = negative ? -1 : 1;
    this._second += increment;

    if (this._second >= secondsInMoment) {
      this._second = 0;
      this.addMoment(negative);
    } else if (this._second < 0) {
      this._second = secondsInMoment - 1;
      this.addMoment(negative);
    }
  }

  // equivalent to addSecond(), but less overhead
  public tick() {
    if (++this._second >= secondsInMoment) {
      this._second = 0;
      this.addMoment();
    }
    ++this.delta;
    return this;
  }

  public toString(): string {
    return `Tridate (${TriWeekday[this.day].slice(0, 3)}, ${this.year}-${
      this.month
    }-${this.date} ${this.hour
      .toString()
      .padStart(2, "0")}:${this.moment
      .toString()
      .padStart(2, "0")}:${this.second.toString().padStart(2, "0")})`;
  }
}

class TickingTridate extends Tridate {}

export {
  secondsInMoment,
  momentsInHour,
  hoursInDay,
  daysInWeek,
  daysByMonth,
  daysByMonthOnLeap,
  daysInYear,
  leapDays,
  daysInCycle,
  yearsInCycle,
  TriMonth,
  TriWeekday,
  Tridate
};
