import {
  Tridate,
  TriMonth,
  secondsInMoment,
  momentsInHour,
  hoursInDay
} from "@/Tritime";
import modulo from "@/modulo";

/**
 * Crea, Smallest Moon: 28 days 3.5 per phase
 * Edalia, Portal Moon: 37 Days 4.625 per phase
 * Byrien, Terraformed Moon: 65 Days 8.125 per phase
 *
 * All moons at full on 32nd of Eighth 1228
 */

enum MoonPhase {
  "Full Moon",
  "Waning Gibbous",
  "Last Quarter",
  "Waning Crescent",
  "New Moon",
  "Waxing Crescent",
  "First Quarter",
  "Waxing Gibbous"
}

enum MoonEmoji {
  "🌕",
  "🌖",
  "🌗",
  "🌘",
  "🌑",
  "🌒",
  "🌓",
  "🌔"
}

function getMoons(date: Tridate) {
  const epoch = new Tridate(1228, TriMonth.Shayagor, 32);
  const days =
    (date.getTime() - epoch.getTime()) /
    (secondsInMoment * momentsInHour * hoursInDay);

  return {
    crea: Math.floor((modulo(days, 28) / 28) * 8),
    edalia: Math.floor((modulo(days, 37) / 37) * 8),
    byrien: Math.floor((modulo(days, 65) / 65) * 8)
  };
}

export { getMoons, MoonPhase, MoonEmoji };
