import mod from "@/modulo";

describe("mod", () => {
  it("0/+", () => {
    expect(mod(0, 3)).toBe(0);
  });

  it("+/+", () => {
    expect(mod(0.5, 3)).toBeCloseTo(0.5);
    expect(mod(1, 3)).toBe(1);
    expect(mod(1.5, 3)).toBeCloseTo(1.5);
    expect(mod(2, 3)).toBe(2);
    expect(mod(2.5, 3)).toBeCloseTo(2.5);
    expect(mod(3, 3)).toBe(0);
    expect(mod(3.5, 3)).toBeCloseTo(0.5);
    expect(mod(4, 3)).toBe(1);
    expect(mod(4.5, 3)).toBeCloseTo(1.5);
    expect(mod(5, 3)).toBe(2);
    expect(mod(5.5, 3)).toBeCloseTo(2.5);
    expect(mod(6, 3)).toBe(0);
  });

  it("-/+", () => {
    expect(mod(-0.5, 3)).toBeCloseTo(2.5);
    expect(mod(-1, 3)).toBe(2);
    expect(mod(-1.5, 3)).toBeCloseTo(1.5);
    expect(mod(-2, 3)).toBe(1);
    expect(mod(-2.5, 3)).toBeCloseTo(0.5);
    expect(mod(-3, 3)).toBe(0);
    expect(mod(-3.5, 3)).toBeCloseTo(2.5);
    expect(mod(-4, 3)).toBe(2);
    expect(mod(-4.5, 3)).toBeCloseTo(1.5);
    expect(mod(-5, 3)).toBe(1);
    expect(mod(-5.5, 3)).toBeCloseTo(0.5);
    expect(mod(-6, 3)).toBe(0);
  });

  it("+/-", () => {
    expect(mod(0.5, -3)).toBeCloseTo(-2.5);
    expect(mod(1, -3)).toBe(-2);
    expect(mod(1.5, -3)).toBeCloseTo(-1.5);
    expect(mod(2, -3)).toBe(-1);
    expect(mod(2.5, -3)).toBeCloseTo(-0.5);
    expect(mod(3, -3)).toBe(-0);
    expect(mod(3.5, -3)).toBeCloseTo(-2.5);
    expect(mod(4, -3)).toBe(-2);
    expect(mod(4.5, -3)).toBeCloseTo(-1.5);
    expect(mod(5, -3)).toBe(-1);
    expect(mod(5.5, -3)).toBeCloseTo(-0.5);
    expect(mod(6, -3)).toBe(-0);
  });

  it("-/-", () => {
    expect(mod(-0.5, -3)).toBeCloseTo(-0.5);
    expect(mod(-1, -3)).toBe(-1);
    expect(mod(-1.5, -3)).toBeCloseTo(-1.5);
    expect(mod(-2, -3)).toBe(-2);
    expect(mod(-2.5, -3)).toBeCloseTo(-2.5);
    expect(mod(-3, -3)).toBe(-0);
    expect(mod(-3.5, -3)).toBeCloseTo(-0.5);
    expect(mod(-4, -3)).toBe(-1);
    expect(mod(-4.5, -3)).toBeCloseTo(-1.5);
    expect(mod(-5, -3)).toBe(-2);
    expect(mod(-5.5, -3)).toBeCloseTo(-2.5);
    expect(mod(-6, -3)).toBe(-0);
  });
});
