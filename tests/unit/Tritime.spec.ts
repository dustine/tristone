// import { shallowMount } from "@vue/test-utils";
import range from "lodash/range";
import sum from "lodash/sum";

import {
  Tridate,
  TriMonth,
  TriWeekday,
  secondsInMoment,
  momentsInHour,
  hoursInDay,
  daysByMonth,
  daysInWeek,
  daysInYear,
  leapDays,
  yearsInCycle,
  daysInCycle,
  daysByMonthOnLeap
} from "@/Tritime";
import modulo from "@/modulo";

const secondsInDay = secondsInMoment * momentsInHour * hoursInDay;

describe("Measures", () => {
  it("number of days equals daysByMonth", () => {
    expect(sum(daysByMonth)).toBe(daysInYear);
  });
});

describe("Tridate Epoch", () => {
  // Epoch:  2013-03-19 15:00:00 ??? <-> 1229-9-01 00:00:00 Adr (Mon)
  it("defaults to the epoch", () => {
    const time = new Tridate();
    expect(time.second).toEqual(0);
    expect(time.moment).toEqual(0);
    expect(time.hour).toEqual(0);
    expect(time.date).toEqual(1);
    expect(time.day).toEqual(TriWeekday.Adraki);
    expect(time.month).toEqual(TriMonth.Azgral);
    expect(time.year).toEqual(1229);
    // epochs always need to be leap-years, for ease of math
    expect(time.isLeapYear()).toBe(true);
  });

  it("constructs the epoch", () => {
    expect(new Tridate()).toEqual(new Tridate(1229, TriMonth.Azgral));
    expect(new Tridate()).toEqual(
      new Tridate(1229, TriMonth.Azgral, 1, 0, 0, 0)
    );
  });

  it("matches new year on 2013", () => {
    // so that 2013-01-01 00:00:00 Tue <-> 1228-1-01 00:00:00 Tyr (Tue)
    const time = Tridate.fromTerran(new Date(2013, 0, 1, 0, 0, 0));
    expect(time).toEqual<Tridate>(
      new Tridate(1228, TriMonth.Ghorza, 1, 0, 0, 0)
    );
    expect(time.day).toEqual(TriWeekday.Tyriel);
    expect(time.isLeapYear()).toBe(false);
  });
});

// describe("equinoxes", () => {
//   console.log(daysInCycle / yearsInCycle);
//   console.log(
//     range(yearsInCycle + 2)
//       .map(
//         v =>
//           v * (secondsInDay * (daysInYear + 0.66)) +
//           secondsInMoment * momentsInHour * 12 +
//           secondsInMoment * 16 +
//           45
//       )
//       .map(v => new Tridate(v).toString())
//   );
// });

describe("Tridate ticks forward", () => {
  const epoch = new Tridate();

  it("+1 second", () => {
    const time = new Tridate(1);
    expect(time).toEqual(
      new Tridate(epoch.year, epoch.month, epoch.date, 0, 0, 1)
    );
  });

  it("+1 moment", () => {
    const time = new Tridate(secondsInMoment);
    expect(time).toEqual(
      new Tridate(epoch.year, epoch.month, epoch.date, 0, 1)
    );
  });

  it("+1 hour", () => {
    const time = new Tridate(secondsInMoment * momentsInHour);
    expect(time).toEqual(new Tridate(epoch.year, epoch.month, epoch.date, 1));
  });

  it("+1 day", () => {
    const time = new Tridate(secondsInDay);
    expect(time).toEqual(new Tridate(epoch.year, epoch.month, epoch.date + 1));
  });

  const weeks = range(1, (daysInCycle + daysInWeek) / daysInWeek).flatMap(w => {
    const weekdays = range(daysInWeek);
    return weekdays.map(d => [w, d]);
  });

  it.each(weeks)("+%i week(s), starting on %i", (weeks, start) => {
    const step = secondsInDay * daysInWeek * weeks + start * secondsInDay;
    const time = new Tridate(step);
    expect(time.day).toBe(start);
  });

  it.each(range(1, 103))("+%i cycle(s)", cycles => {
    const time = new Tridate(secondsInDay * daysInCycle * cycles);
    expect(time).toEqual(
      new Tridate(epoch.year + cycles * yearsInCycle, epoch.month, epoch.date)
    );
  });
});

describe("Tridate days", () => {
  const epoch = new Tridate();

  const daysOnLeap = daysByMonthOnLeap.flatMap((val, rawm) => {
    const m = rawm == 0 ? 9 : ((rawm + 1) % (daysByMonth.length - 1)) + 1;
    return range(1, val + 1).map(d => [m, d]);
  });

  const daysAfterLeap = daysByMonth.flatMap((val, rawm) => {
    const m = rawm == 0 ? 2 : ((rawm + 1) % (daysByMonth.length - 1)) + 1;
    return range(1, val + 1).map(d => [
      m,
      rawm == 0 ? d + daysByMonth[daysByMonth.length - 1] : d
    ]);
  });

  const daysOnCycle = daysOnLeap
    .map((val, i) => [i, ...val, epoch.year])
    .slice(1)
    .concat(
      range(1, yearsInCycle).flatMap(rawy => {
        return daysAfterLeap.map((val, i) => [
          i + rawy * daysInYear + leapDays,
          ...val,
          rawy + epoch.year
        ]);
      })
    );

  it.each(daysOnCycle)("+%i day(s), on %i/%i/%i", (days, month, day, year) => {
    const time = new Tridate(days * secondsInDay);
    expect(time).toEqual(new Tridate(year, month, day));
  });

  const daysOnReverseCycle = range(0, -yearsInCycle + 1, -1)
    .flatMap(rawy => {
      const y = rawy + epoch.year - 1;
      return daysAfterLeap
        .slice()
        .reverse()
        .map((val, i) => [-i + rawy * daysInYear - 1, ...val, y]);
    })
    .concat(
      daysOnLeap
        .slice()
        .reverse()
        .map((val, i) => [
          -i - (yearsInCycle - 1) * daysInYear - 1,
          ...val,
          epoch.year - yearsInCycle
        ])
    );

  it.each(daysOnReverseCycle)(
    "%i day(s), on %i/%i/%i",
    (days, month, day, year) => {
      const time = new Tridate(days * secondsInDay);
      expect(time).toEqual(new Tridate(year, month, day));
    }
  );
});

// describe("Tridate ticks backwards", () => {
//   const epoch = new Tridate();

//   const offByOne = new Tridate(-1)

//   it.each(_.range(1, secondsInMoment))("-%i second(s)", seconds => {
//     const time = new Tridate(undefined, -seconds);
//     expect(time).toEqual(
//       new Tridate(offByOne.year)
//     );
//     expect(time.values).toEqual({
//       second: secondsInMoment - seconds,
//       moment: offByOne.moment,
//       hour: offByOne.hour,
//       day: offByOne.day,
//       weekday: offByOne.weekday,
//       month: offByOne.month,
//       year: offByOne.year
//     });
//   });

//   it.each(_.range(1, momentsInHour))("-%i moment(s)", moments => {
//     const step = secondsInMoment * moments;
//     const time = new Tridate(undefined, -step);
//     expect(time.values).toEqual({
//       second: 0,
//       moment: secondsInMoment - moments,
//       hour: offByOne.hour,
//       day: offByOne.day,
//       weekday: offByOne.weekday,
//       month: offByOne.month,
//       year: offByOne.year
//     });
//   });

//   it.each(_.range(1, hoursInDay))("-%i hour(s)", hours => {
//     const step = secondsInMoment * momentsInHour * hours;
//     const time = new Tridate(undefined, -step);
//     expect(time.values).toEqual({
//       second: 0,
//       moment: 0,
//       hour: hoursInDay - hours,
//       day: offByOne.day,
//       weekday: offByOne.weekday,
//       month: offByOne.month,
//       year: offByOne.year
//     });
//   });

//   const weeks = _.range(1, daysInCycle / daysInWeek).flatMap(w => {
//     const weekdays = _.range(daysInWeek).reverse();
//     return weekdays.map(d => [w, d]);
//   });

//   it.each(weeks)("-%i week(s), starting on %i", (weeks, start) => {
//     const step = secondsInDay * daysInWeek * weeks + start * secondsInDay;
//     const time = new Tridate(undefined, step);
//     expect(time.weekday).toBe(start);
//   });

//   it.each(_.range(1, 103))("-%i cycle(s)", () => {
//     const time = new Tridate(undefined, secondsInDay * daysInCycle);
//     expect(time.values).toEqual({
//       second: epoch.second,
//       moment: epoch.moment,
//       hour: epoch.hour,
//       day: epoch.day,
//       weekday: time.weekday, // ignore day of the week, it's properly tested elsewhere
//       month: epoch.month,
//       year: epoch.year + yearsInCycle
//     });
//     expect(time.year).toBeGreaterThan(0);
//   });
// });
